import { Canvas, export_game, Game, Options } from "./lib/mod.ts"

const rows = 4
const columns = 4
const connect_len = 4

interface State {
    turn: boolean,
    board: boolean[][]
    win?: boolean
}

const char_of = (p: boolean): string => (p ? "X" : "O")

function set_winner(s: State) {
    for (const pl of [true, false]) {
        for (const col of s.board) {
            let mr = 0
            for (let i = 1; i < col.length; i++) {
                if ((col[i] == pl) && (col[i - 1] == pl))
                    mr++
                if (mr >= connect_len - 1) return s.win = pl
            }
        }
        for (let r = 0; r < rows; r++) {
            let mr = 0
            for (let i = 1; i < columns; i++) {
                if ((s.board[i][r] == pl) && (s.board[i - 1][r] == pl))
                    mr++
                if (mr >= connect_len - 1) return s.win = pl
            }
        }
    }
}

const connect4: Game<State> = {
    step: (s) => {
        if (s.win !== undefined) return {} // "r": { name: "restart game", run: () => connect4.initial() } }
        const o: Options<State> = {}
        for (let i = 0; i < columns; i++) {
            if (s.board[i].length >= rows) continue
            o[`${i + 1}`] = {
                name: `throw a ${char_of(s.turn)} in the ${i + 1}. column.`,
                update: (s) => {
                    s.board[i].push(s.turn)
                    s.turn = !s.turn
                    set_winner(s)
                }
            }
        }
        return o
    },
    draw: (s) => {
        if (s.win !== undefined) return `${char_of(s.win)} won the game`
        const c = new Canvas(columns + 2, rows + 1)
        for (let i = 0; i < rows; i++) {
            c.setChar(0, i, "|")
            c.setChar(columns + 1, i, "|")
        }
        for (let i = 0; i < columns; i++) {
            c.setChar(i + 1, rows, (i + 1).toString().substr(0, 1))
        }
        for (let col = 0; col < columns; col++) {
            for (let rs = 0; rs < s.board[col].length; rs++) {
                const p = s.board[col][rs];
                c.setChar(col + 1, rows - 1 - rs, char_of(p))
            }
        }
        return c.export()
    },
    initial: () => {
        return {
            turn: false,
            board: new Array(columns).fill([])
        }
    }
}

export_game("/tmp/game", connect4)
