import hashJs from 'https://deno.land/x/hash/mod-hashjs.ts'
import crc32 from 'https://deno.land/x/hash/mod-crc32.ts'


export class Canvas {
    w: number
    h: number
    buf: string[][] = []

    constructor(w: number, h: number) {
        this.w = w; this.h = h
        for (let y = 0; y < h; y++) {
            this.buf.push(new Array(w).fill(" "))
        }
    }
    export(): string { return this.buf.map(l => l.join("")).join("\n") }

    drawString(x: number, y: number, s: string) {
        for (let i = 0; i < s.length; i++) {
            this.setChar(x + i, y, s[i])
        }
    }

    setChar(x: number, y: number, char: string) {
        this.buf[y][x] = char
    }
}

export interface Export {
    [key: string]: Screen
}
export interface Screen {
    text: string,
    options: {
        [key: string]: {
            name: string,
            target: string
        }
    }
}

// deno-lint-ignore no-explicit-any
function hash(thing: any): string {
    return crc32.str(JSON.stringify(thing))
    // return hashJs.sha256().update(JSON.stringify(thing)).digest('hex')
}

// deno-lint-ignore no-explicit-any
const dlog = (s: any) => Deno.stdout.writeSync(new TextEncoder().encode("\x1b[2K" + s.toString() + "\r"));

export interface Options<S> {
    [key: string]: {
        name: string,
        update?: (state: S) => void
        replace?: (state: S) => S,
    }
}

export interface Game<S> {
    step(state: S): Options<S>
    draw(state: S): string
    initial(): S
}

export async function export_game<S>(filename: string, game: Game<S>) {
    const e = new Map<string, Screen>()
    let counter = 0
    let iterCount = 0

    let queue: S[] = [game.initial()]
    let queue2: S[] = []

    while (queue.length) {
        dlog(`depth ${iterCount++}, states queued: ${queue.length}`)
        while (queue.length) {
            const s = queue.pop()
            if (!s) throw new Error("asdf");
            const h = hash(s)
            if (e.get(h)) continue
            const branches = game.step(s);
            const options: { [key: string]: { name: string, target: string } } = {}
            Object.entries(branches).forEach(([key, o]) => {
                let s2 = JSON.parse(JSON.stringify(s))
                if (o.replace) s2 = o.replace(s2)
                if (o.update) o.update(s2)
                options[key] = { name: o.name, target: hash(s2) }
                queue2.push(s2)
            })
            e.set(h, {
                text: game.draw(s),
                options
            })
            counter++;
            if (counter % 31437 == 0) dlog(`rendering states: ${counter}`)
        }
        queue = queue2
        queue2 = []
    }

    const start = e.get(hash(game.initial()))
    if (!start) throw new Error("fsdkjls");
    e.set("start", start)

    dlog(`exporting object`)
    const exp: Export = {}
    e.forEach((value, key) => {
        exp[key] = value
    })

    dlog(`writing to file`)
    await Deno.writeTextFile(filename, JSON.stringify(exp))
    dlog(`done`)
}