import { Game, Options } from "./mod.ts";

// deno-lint-ignore no-explicit-any
export type StateManagerState = { inner: any, name: string }

// deno-lint-ignore no-explicit-any
export function stateManager(subgames: Record<string, Game<any>>, initialSubgame: string): Game<StateManagerState> {
    return {
        initial: () => ({ name: initialSubgame, inner: subgames[initialSubgame].initial() }),
        draw: s => subgames[s.name].draw(s.inner),
        step: s => {
            function insertName<T>(o: Options<T>): Options<StateManagerState> {
                //@ts-ignore bad code
                return Object.fromEntries(Object.entries(o).map(([key, value]) => {
                    return [key, {
                        name: value.name,
                        update: value.update ? s => {
                            //@ts-ignore bad code
                            value.update(s.inner)
                            // deno-lint-ignore no-explicit-any
                            const r: any = s.inner
                            if (r.transition) {
                                //@ts-ignore bad code
                                s.name = r.transition
                                s.inner = subgames[r.transition].initial()
                            }
                        } : undefined,
                        //@ts-ignore bad code
                        replace: value.replace ? s => {
                            //@ts-ignore bad code
                            // deno-lint-ignore no-explicit-any
                            const r: any = value.replace(s.inner)
                            if (r.transition) return { name: r.transition, inner: subgames[r.transition].initial() }
                            return { name: s.name, inner: r }
                        } : undefined
                        // TODO
                    }]
                }))
            }
            return insertName(subgames[s.name].step(s.inner))
        }
    }
}
