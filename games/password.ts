import { export_game, Game, Options } from "./lib/mod.ts"

interface State {
    len: number,
    correct: boolean
    submit: boolean
}

const password = "123"
const max_len = 32

const app: Game<State> = {
    initial: () => ({ len: 0, correct: true, submit: false }),
    draw: (s) => s.submit
        ? (s.correct
            ? "Password correct!"
            : "Password incorrect")
        : `Password: ${"*".repeat(s.len)}`,
    step: s => {
        if (s.submit) return { "r": { name: "Restart", replace: () => app.initial() } }
        const opts: Options<State> = {}
        if (s.len < max_len) {
            for (const c of "0123456789".split("")) {
                opts[c] = {
                    name: `type '${c}'`,
                    update: s => {
                        s.correct &&= c == password[s.len]
                        s.len += 1
                    }
                }
            }
        }
        opts["p"] = {
            name: "submit",
            update: s => {
                s.submit = true,
                    s.len = 0
            }
        }
        return opts
    },
}

export_game("/tmp/game", app)