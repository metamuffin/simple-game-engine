import { Canvas, export_game, Game, Options } from "./lib/mod.ts"
import { stateManager } from "./lib/statemanager.ts";

interface Player {
    x: number,
    y: number
}

interface TitleScreenState {
    screen: "start" | "credits" | "exit",
    transition?: string
}
interface IngameState {
    transition?: string
    player: Player,
    dialog?: string
    dead?: boolean
}

function loadMap(raw: string): string[][] {
    return raw.trim().split("\n").map(l => l.split(""))
}

const map = loadMap(`
...............................
...#####.......................
...#...#...........xxxxxx......
...#N...................x......
...#...#...........xxxx.x......
...#####..............xMx......
......................xxx......
...............................
...............................
...............................
`)
const mapSize = { x: map[0].length, y: map.length }
const viewportSize = { x: 20, y: 10 }

export interface Npc { dialog: string }
const npcs: Map<string, Npc> = new Map()
npcs.set("N", { dialog: "Hello world!" })
npcs.set("M", { dialog: "What are you doing?" })

function mapAt(x: number, y: number): string {
    if (x < 0 || x >= mapSize.x) return "#"
    if (y < 0 || y >= mapSize.y) return "#"
    return map[y][x]
}
function collide(t: string): boolean {
    return t != "." && t != "x"
}
function renderMapAt(s: IngameState, x: number, y: number): string {
    if (s.player.x == x && s.player.y == y) return "P"
    return mapAt(x, y)
}
function npcNearby({ x, y }: { x: number, y: number }): Npc | undefined {
    for (let xo = -1; xo <= 1; xo++) {
        for (let yo = -1; yo <= 1; yo++) {
            const c = mapAt(x + xo, y + yo)
            const n = npcs.get(c)
            if (n) return n
        }
    }
    return undefined
}

const titleScreen: Game<TitleScreenState> = {
    step: s => {
        const o: Options<TitleScreenState> = {}
        if (s.screen == "start") {
            o["1"] = { name: "Start game", update: s => s.transition = "ingame" }
            o["2"] = { name: "Credits", update: s => s.screen = "credits" }
            o["3"] = { name: "Exit", update: s => s.screen = "exit" }
        } else o["b"] = { name: "Back", update: s => s.screen = "start" }
        return o;
    },
    draw: s => {
        const screens = {
            "start": "RPG Demo for\n\tthe simple game engine",
            "credits": "Made for the simple game engine\nDeveloped by: metamuffin\nCo-authored by: potatoxel\n\nThanks for playing!",
            "exit": "You cannot!"
        }
        return screens[s.screen]
    },
    initial: () => ({ screen: "start" })
}

const rpg: Game<IngameState> = {
    step: s => {
        const o: Options<IngameState> = {}
        const standinOn = mapAt(s.player.x, s.player.y)
        if (!npcNearby(s.player)) s.dialog = undefined
        if (standinOn == "x") s.dead = true
        if (s.dead) return {
            "r": { name: "Respawn", replace: () => rpg.initial() },
            "t": { name: "Quit to title screen", update: s => s.transition = "title" }
        }
        if (!collide(mapAt(s.player.x, s.player.y - 1))) o["w"] = { name: "walk up", update: s => s.player.y -= 1 }
        if (!collide(mapAt(s.player.x, s.player.y + 1))) o["s"] = { name: "walk down", update: s => s.player.y += 1 }
        if (!collide(mapAt(s.player.x - 1, s.player.y))) o["a"] = { name: "walk left", update: s => s.player.x -= 1 }
        if (!collide(mapAt(s.player.x + 1, s.player.y))) o["d"] = { name: "walk right", update: s => s.player.x += 1 }
        if (npcNearby(s.player)) o["e"] = { name: "talk to npc", update: s => s.dialog = npcNearby(s.player)?.dialog }
        return o
    },
    draw: s => {
        const c = new Canvas(viewportSize.x, viewportSize.y)
        for (let x = 0; x < viewportSize.x; x++) {
            for (let y = 0; y < viewportSize.y; y++) {
                c.setChar(x, y, renderMapAt(s, x + s.player.x - viewportSize.x / 2, y + s.player.y - viewportSize.y / 2))
            }
        }
        if (s.dialog) c.drawString(1, viewportSize.y - 2, s.dialog)
        if (s.dead) c.drawString(1, viewportSize.y - 2, "[ You died ]")
        return c.export()
    },
    initial: () => {
        return { player: { x: 13, y: 3 } }
    }
}

export_game("/tmp/game", stateManager({ title: titleScreen, ingame: rpg }, "title"))

